# minizip-clone

This is the demo project for the gitlab pipeline. 

The origin minizip-ng is here. [https://github.com/zlib-ng/minizip-ng](https://github.com/zlib-ng/minizip-ng) by Nathan Moinvaziri.

I just add the gitlab pipeline to the project. If you want to improve your skills, you can read the github action workflow and try to write the pipeline for it.

## Workflow

* build
* test
* coverage
* deliver


### Build

Compile the source code. Here you can use any build tool as you want.

* ninja
* make
* cmake
* maven

For cmake, we need to create `build` folder and choose configuration and options.

```bash
cmake -S . -B build -D MZ_BUILD_TESTS=ON ...
```

And the compile the source code.

```bash
cmake --build build --config 'Release'
```

### Test

Perform unit tests. Here we use the google test.

```bash
cmake --output-on-failure -C 'Release'
```

### Coverage

Create coverage report after finishing the unit test.

```bash
python3 -u -m pip install --user gcovr --ignore-installed
python3 -m gcovr \
    --exclude-unreachable-branches \
    --gcov-ignore-parse-errors \
    --gcov-executable "gcov" \
    --root . \
    --html \
    --output coverage.html \
    --verbose
```

The report will be the artifact and download from gitlab.


### Deliver

Generate the artifact. 

Let gitlab create the artifact for you.



## Before you perform the build automation...

1. Know how to setup your build or test environment, the operating systems, the dependencies, the compilers, the environment variables...

2. Make sure your build tools and build scripts are easy to use and readable. Well the same for your test framework and test scripts. The pipeline only make the commands to the build bools. 

